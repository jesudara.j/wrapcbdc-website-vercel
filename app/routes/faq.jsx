import { Card, Col, Collapse, Input, Row } from "antd";
import BaseLayout from "~/components/baseLayout";
const { TextArea } = Input;
const { Panel } = Collapse;



export default () => {
  return (
    <>
      <BaseLayout>
        <Row gutter={[16, 64]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
          <Col xs={{span: 24}} lg={{span: 10}} style={{height: "100%"}}>
            {/* <img src="/contact_assets/contact_hero.png" width={400} height={400} /> */}
          </Col>
          <Col xs={{span: 24}} lg={{span: 14}} flex style={{height: "100%", display: "flex", flexFlow: "column", alignItems: "end", justifyContent: "center"}}>
            <h1 className="faq-hero-title">FAQ</h1>
            <span className="faq-hero-subtitle">You have questions?<br/>We have answers.</span>
            <br/>
            
          </Col>
          <Col xs={{span: 24}} lg={{span: 24}} style={{ height: "100%" }}>
            <Card
              style={{
                background: "transparent",
                // border: 0,
                // padding: 40,
                borderRadius: 10,
                // boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
              }}
              
            >
              <Collapse
                defaultActiveKey={["1"]}
                onChange={() => {}}
                expandIconPosition={"right"}
                bordered={false}
                className="sec4-collapse"
              >
                <Panel
                  header="What is Convexity token?"
                  key="1"
                  className="sec4-panel"
                >
                  <div></div>
                </Panel>
                <Panel header="How does it work?" key="2">
                  <div></div>
                </Panel>
                <Panel header="Who can use it?" key="3">
                  <div></div>
                </Panel>
                <Panel
                  header="How does Convexity wrap token protect me from crypto volatility?"
                  key="4"
                >
                  <div></div>
                </Panel>
                <Panel
                  header="How do i know my  Convexity wrap token are secured?"
                  key="5"
                >
                  <div></div>
                </Panel>
                <Panel header="Is Convexity wrap token transparent?" key="6">
                  <div></div>
                </Panel>
                <Panel header="What currencies and commodities does Convexity wrap token support?" key="7">
                  <div></div>
                </Panel>
                <Panel header="How are Convexity wrap token created?" key="8">
                  <div></div>
                </Panel>
                <Panel header="Where can i use Convexity wrap tokens?" key="9">
                  <div></div>
                </Panel>
                <Panel header="How often does Convexity wrap token do Audit?" key="10">
                  <div></div>
                </Panel>
                <Panel header="What is the registration process to become a customer?" key="11">
                  <div></div>
                </Panel>
                <Panel header="What blockchain supports the Convexity wrap token?" key="12">
                  <div></div>
                </Panel>
                {/* <Panel header="Is Convexity wrap token transparent?" key="3">
                  <div></div>
                </Panel>
                <Panel header="Is Convexity wrap token transparent?" key="3">
                  <div></div>
                </Panel> */}
              </Collapse>
            </Card>
          </Col>
        </Row>
      </BaseLayout>
      {/* FAQ  */}
      {/* <Faq /> */}
    </>
  );
};
