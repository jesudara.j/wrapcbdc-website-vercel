import { useOutletContext } from "remix";
import Faq from "~/components/faq";
import Hero from "~/components/hero";
import Section2 from "~/components/section2";
import Section3 from "~/components/section3";



export default () => {
  const { screens, isMobile } = useOutletContext();
  return (
    <>
    <Hero screens={screens} isMobile={isMobile} />
    {/* ENDLESS BOUNDARY SECTION */}
    <Section2 isMobiel={isMobile} />

    {/* TRANSPARENTCY/SWAP SOLUTION */}
    <Section3 />
    {/* FAQ  */}
    <Faq />
    </>
  );
};
