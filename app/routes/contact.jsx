import { Button, Col, Input, Row } from "antd";
import { useOutletContext } from "remix";
import BaseLayout from "~/components/baseLayout";
import Faq from "~/components/faq";
const { TextArea } = Input;



export default () => {
  const { screens, isMobile } = useOutletContext();
  return (
    <>
      <BaseLayout extraStyle={{backgroundImage: "url('https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_1649/v1646896289/world_map_usjcvp.png')",
        backgroundPosition: "center 150px",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat"}}>
        <Row gutter={[16, 64]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
          <Col xs={{span: 24}} lg={{span: 12}} style={{height: "100%"}}>
            <img src="https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_920/v1646897700/contact_hero_xe8c8u.png" width={isMobile? '100%' : 400} height={isMobile? '100%' : 400} />
          </Col>
          <Col xs={{span: 24}} lg={{span: 12}} flex style={{height: "100%", display: "flex", flexFlow: "column", alignItems: "start", justifyContent: "center", paddingTop: "5%"}}>
            <h1 className="contact-hero-title">CONTACT US</h1>
            <span className="contact-hero-subtitle">Do you have a specific requirement, or need help in integration
             or would like to strike a partnership with us? Our team is ready to respond to any of your inquiries.</span>
            <br/>
            <div style={{display: "flex", alignItems: "center", justifyContent: "start"}}>
              <Button type="link" size="large" style={{backgroundColor: "white", color: "black", marginRight: 10}} href="https://wrapcbdc-dashboard-staging.herokuapp.com/">SIGN IN</Button>
              <Button type="link" size="large" style={{backgroundColor: "black", color: "white", border: "1px solid white", marginRight: 10}} href="https://wrapcbdc-dashboard-staging.herokuapp.com/">GET CNGN</Button>
            </div>
          </Col>
          <Col xs={{span: 24}} lg={{span: 24}} style={{paddingLeft: isMobile? 5 : 100, paddingRight: isMobile? 5 : 100}}>
            <Input.Group size="large">
              <Row gutter={[16, 16]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
                <Col span={12}>
                  <Input type="text" placeholder="Full Name" />
                </Col>
                <Col span={12}>
                  <Input type="text" placeholder="Subject" />
                </Col>
                <Col span={12}>
                  <Input type="tel" placeholder="Phone" />
                </Col>
                <Col span={12}>
                  <Input type="email" placeholder="Email*" />
                </Col>
                <Col span={24}>
                  <TextArea rows={6} placeholder="How may we be of help?*" />
                </Col>
                <Col span={24}>
                  <Button type="link" size="large" style={{backgroundColor: "white", color: "black"}} block>SEND MESSAGE</Button>
                </Col>
              </Row>
            </Input.Group>
          </Col>
        </Row>
      </BaseLayout>
      {/* FAQ  */}
      <Faq />
    </>
  );
};
