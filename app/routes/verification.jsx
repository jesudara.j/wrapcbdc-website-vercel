import { Card, Col, Collapse, Input, Row } from "antd";
import BaseLayout from "~/components/baseLayout";
const { TextArea } = Input;
const { Panel } = Collapse;



export default () => {
  return (
    <>
      <BaseLayout>
        <Row gutter={[16, 64]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
          <Col xs={{span: 24}} lg={{span: 10}} style={{height: "100%"}}>
            {/* <img src="/contact_assets/contact_hero.png" width={400} height={400} /> */}
          </Col>
          <Col xs={{span: 24}} lg={{span: 14}} flex style={{height: "100%", display: "flex", flexFlow: "column", alignItems: "end", justifyContent: "center"}}>
            <h1 className="faq-hero-title">VERIFICATION</h1>
            <span className="faq-hero-subtitle">How to Verify your account</span>
            <br/>
            
          </Col>
          <Col xs={{span: 24}} lg={{span: 24}} style={{ height: "100%" }}>
            <Card
              style={{
                background: "transparent",
                // border: 0,
                // padding: 40,
                borderRadius: 10,
                // boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
              }}
              
            >
              <p className="verification-p">NOTE: Verification is the procedure of setting up an account holder’s non-public or company identity. to use Convexity wrap token offerings after setting up an account, you have to first finish an account verification technique.</p>
              <ol className="verification-list">
                <li>To get started, sign in to your Convexity wrap token account.</li>
                <li>Then, go to the Verification page.<br/>Important: Individual verification is valid if you intend to use your Convexity wrap account for personal purposes, while Business Verification is available for businesses and companies. Note: Before proceeding to the next step in the verification process, read all the requirements carefully and check that you have all the required documents.</li>
                <li>Next, select your account type between Individual and Business.</li>
                <li>After determining your eligibility for confirmation visit the deposit page, and follow the on-screen instructions to pay the appropriate confirmation fee. Important: Before proceeding read carefully our fee page to check the required verification fees and other related information.</li>
                <li>Once you have paid the verification fee at one of the created addresses, you will receive an email confirming that the deposit has been received in your account.</li>
                <li>Finally, you can proceed to complete the verification stages. Once you have submitted them in full, our compliance team will begin reviewing your request. Note: This is a manual procedure that can take anywhere from a few days to a few weeks to complete.</li>
                
              </ol>
              <p className="verification-p">Our team will contact you by email if your confirmation is successful or if there is any additional information to complete your verification.</p>
              <p className="verification-p">If you have any questions about this process feel free to contact Convexity wrap token Support.</p>
            </Card>
          </Col>
        </Row>
      </BaseLayout>
      {/* FAQ  */}
      {/* <Faq /> */}
    </>
  );
};
