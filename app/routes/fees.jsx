import { Button, Card, Col, Input, Row } from "antd";
import BaseLayout from "~/components/baseLayout";
const { TextArea } = Input;



export default () => {
  return (
    <>
      <BaseLayout>
        <Row justify="space-around" align="middle" gutter={[16, 64]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
          <Col xs={{span: 24}} lg={{span: 10}} style={{height: "100%"}}>
            
          </Col>
          <Col xs={{span: 24}} lg={{span: 14}} flex style={{height: "100%", display: "flex", flexFlow: "column", alignItems: "end", justifyContent: "center"}}>
            <h1 className="fees-hero-title">FEES</h1>
            <span className="fees-hero-subtitle">* This information given below is the current minimum amount required for a fiat deposit or redemption..</span>
            <br/>
          </Col>
        </Row>
        <Row gutter={[16, 16]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0, marginBottom: 20}}>
          <Col xs={{span: 24}} lg={{span: 12}} style={{ display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "auto"}}>
            <Card
              style={{
                background: "transparent",
                // border: 0,
                borderRadius: 10,
                height: "100%",
                width: "100%",
                // padding: 40,
                // boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
              }}
              title={<span className="sec2-cards-title"><img src="/green-naira.png" width={20} height={20} /> 30,000,000</span>}
            >
              <span className="sec2-card-text">
                Minimum fiat, enaira and Convexity wrapped token withdrawal or deposit
              </span>
            </Card>
          </Col>
          <Col xs={{span: 24}} lg={{span: 12}} style={{ display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "auto" }}>
            <Card
              style={{
                background: "transparent",
                // border: 0,
                borderRadius: 10,
                height: "100%",
                width: "100%",
                // padding: 40,
                // boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
              }}
              title={<span className="sec2-cards-title">0.001</span>}
            >
              <span className="sec2-card-text">
                Fee per fiat and eNaira withdrawal and redemption
              </span>
              {/* <br/> */}
              {/* <br/> */}
            </Card>
          </Col>
        </Row>
        <Row justify="space-around" align="middle" gutter={[16, 64]} style={{width: "100%", marginLeft: 0, marginRight: 0, padding: 0}}>
          <Col xs={{span: 24}} lg={{span: 24}} style={{ height: "100%", paddingTop: 20, paddingBottom: 20 }}>
            <span className="sec2-card-text">Note:  Convexity wrap token does a direct swap of NGN and eNaira to CNGN once you make a deposit</span>
          </Col>
          <Col xs={{span: 24}} lg={{span: 24}} style={{ height: "100%" }}>
            <Card
              style={{
                background: "transparent",
                // border: 0,
                // padding: 40,
                borderRadius: 10,
                // boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
              }}
              title={<span className="sec2-cards-title"><img src="/green-naira.png" width={20} height={20} /> 85,000</span>}
              extra={<Button type="link" size="large" style={{backgroundColor: "white", color: "black", marginRight: 10}} href="https://wrapcbdc-dashboard-staging.herokuapp.com/?view=signup">SIGN UP</Button>}
            >
              <span className="sec2-card-text">
              The verification fee is non-refundable and a one-time payment.
               The fee is intended to ensure that only serious parties who are 
               committed to opening an account apply. The charge imposed also helps
                reduce a portion of costs incurred from the verification service and other 3rd party 
                services. To conform with applicable laws Convexity keeps the sole discretion to approve or 
                not to approve individual or company accounts. 
              </span>
            </Card>
          </Col>
        </Row>
      </BaseLayout>
      {/* FAQ  */}
      {/* <Faq /> */}
    </>
  );
};
