import { Button, Col, Row } from "antd";

export default () => {
  return (
    <Row style={{ width: "100%" }}>
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
      <Col xs={{ span: 22 }} lg={{ span: 18 }}>
        {/* CONTENT GOES HERE */}
        <Row
          gutter={[16, 16]}
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginTop: 140,
            // marginBottom: 0,
            padding: 0,
            paddingBottom: 100,
            width: "100%",
          }}
        >
          <Col
            xs={{ span: 24, order: 2 }}
            lg={{ span: 14, order: 1 }}
            style={{
              height: "100%",
              padding: 0,
              display: "flex",
              alignItems: "center",
              justifyContent: "start",
            }}
          >
            <img
              src="https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_1266/v1646896173/transparency_f6nkx6.png"
              width={"85%"}
              height={"48%"}
            />
          </Col>
          <Col
            xs={{ span: 24, order: 1 }}
            lg={{ span: 10, order: 2 }}
            style={{
              height: "100%",
              display: "flex",
              flexFlow: "column",
              alignItems: "start",
              justifyContent: "space-evenly",
            }}
          >
            <div
              style={{
                display: "flex",
                flexFlow: "column",
                alignItems: "start",
                justifyContent: "center",
              }}
            >
              <h1 className="sec3-title">FULL TRANSPARENCY</h1>
              <span className="sec3-subtitle">
              The value of our reserves is published daily and matches or
               exceeds the value of all tokens in circulation.
              </span>
              <br />
              <br />
              <Row
                gutter={[16, 16]}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Button
                    type="link"
                    size="large"
                    style={{
                      border: "1px solid white",
                      color: "white",
                      marginRight: 15,
                    }}
                  >
                    PROOF OF FUNDS
                  </Button>
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <Button
                    type="link"
                    size="large"
                    style={{ backgroundColor: "white", color: "black" }}
                  >
                    AUDITORS REPORT PDF
                  </Button>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <Row
          gutter={[16, 16]}
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginTop: 60,
            marginBottom: 140,
            padding: 0,
            width: "100%",
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 14 }}
            style={{
              height: "100%",
              display: "flex",
              flexFlow: "column",
              alignItems: "start",
              justifyContent: "space-evenly",
            }}
          >
            <div
              style={{
                display: "flex",
                flexFlow: "column",
                alignItems: "start",
                justifyContent: "center",
              }}
            >
              <h1 className="sec3-title">CBDC TOKEN SWAP SOLUTION</h1>
              <p className="sec3-subtitle">
              Convexity wrapped CBDC, was created because we believe technology should be inclusive.
               When more people are involved in technology, it spurs innovation, helps economies
              grow and expands choices for consumers.
              </p>
              <br />
              <p className="sec3-subtitle">
              Financial interoperability requires a stable price as means of value exchange.
               The first implementation of wrapcbdc is Convexity eNaira(CNGN) which is currently
                available on the Bantu Network as well as Binance Smart Chain.
               CNGN creates possibilities in payments, lending, investing, trading and trade finance.
              </p>
              <br />
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Button
                  type="link"
                  size="large"
                  style={{ backgroundColor: "white", color: "black" }}
                >
                  WHITEPAPER
                </Button>
              </div>
            </div>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 10 }}
            style={{
              height: "100%",
              padding: 0,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img
              src="https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_982/v1646896153/swap_solution_e1vaas.png"
              width={"80%"}
              height={"90%"}
            />
          </Col>
        </Row>
      </Col>
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
    </Row>
  );
};
