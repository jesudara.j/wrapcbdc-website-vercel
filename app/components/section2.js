import { Card, Col, Row } from "antd";

export default ({ isMobile }) => {
  return (
    <Row
      style={{ width: "100%", borderBottom: "1px solid rgba(225,225,225,.3)" }}
    >
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
      <Col xs={{ span: 22 }} lg={{ span: 18 }}>
        {/* CONTENT GOES HERE */}
        <Row
          gutter={[8, 32]}
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginTop: 140,
            marginBottom: 140,
            padding: 0,
            width: "100%",
            borderBottom: "0.5px solid bottom",
          }}
        >
          <Col xs={{ span: 1 }} lg={{ span: 2 }}></Col>
          <Col xs={{ span: 22 }} lg={{ span: 20 }}>
            <Row gutter={[32, 32]}>
              <Col
                xs={{ span: 24 }}
                lg={{ span: 24 }}
                style={{
                  height: "auto",
                  display: "flex",
                  flexFlow: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <h1 className="sec2-title">BOUNDLESS USECASE</h1>
                <br />
                <span className="sec2-subtitle">
                  We give you the benefits of open blockchain technology on
                  traditional currencies by converting your fiat into Convexity
                  wrapped CBDC equivalent.
                </span> 
                <br />
                <br />
              </Col>
              <Col
                xs={{ span: 24 }}
                lg={{ span: 12 }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "auto",
                }}
                flex
              >
                <Card
                  style={{
                    width: "100%",
                    height: "100%",
                    background: "transparent",
                    border: 0,
                    borderRadius: 10,
                    boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
                  }}
                  title={<span className="sec2-cards-title">Swap</span>}
                  extra={
                    <img src="/sec2_icons/swap.png" width={70} height={70} />
                  }
                >
                  <span className="sec2-card-text">
                    Get the Convexity wrapped CBDC pegged 1:1 to fiat and eNaira
                    without stress
                  </span>
                  <br />
                  <br />
                </Card>
              </Col>
              <Col
                xs={{ span: 24 }}
                lg={{ span: 12 }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "auto",
                }}
              >
                <Card
                  style={{
                    width: "100%",
                    height: "100%",
                    background: "transparent",
                    borderRadius: 10,
                    boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
                  }}
                  title={<span className="sec2-cards-title">Stability</span>}
                  extra={
                    <img
                      src="/sec2_icons/stability.png"
                      width={70}
                      height={70}
                    />
                  }
                >
                  <span className="sec2-card-text">
                    Convexity wrapped CBDC has the functionalities of a stable
                    coin, hence hedging against the volatility of your
                    crypto-assets.
                  </span>
                </Card>
              </Col>
              <Col
                xs={{ span: 24 }}
                lg={{ span: 12 }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "auto",
                }}
              >
                <Card
                  style={{
                    width: "100%",
                    height: "100%",
                    background: "transparent",
                    borderRadius: 10,
                    boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
                  }}
                  title={<span className="sec2-cards-title">Trade</span>}
                  extra={
                    <img src="/sec2_icons/trade.png" width={70} height={70} />
                  }
                >
                  <span className="sec2-card-text">
                    Trade Convexity wrapped CBDC on exchanges, markets and OTC
                    desks across the globe.
                  </span>
                </Card>
              </Col>
              <Col
                xs={{ span: 24 }}
                lg={{ span: 12 }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "auto",
                }}
              >
                <Card
                  style={{
                    width: "100%",
                    height: "100%",
                    background: "transparent",
                    border: 0,
                    borderRadius: 10,
                    boxShadow: "rgba(225,225,225,.25) 0px 4px 10px 4px",
                  }}
                  title={<span className="sec2-cards-title">Utility</span>}
                  extra={
                    <img src="/sec2_icons/utility.png" width={70} height={70} />
                  }
                >
                  <span className="sec2-card-text">
                    Stake, farm, mine Convexity wrapped tokens on DeFi Platform
                    like Bantu, BSC and Ethereum for high returns.
                  </span>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xs={{ span: 1 }} lg={{ span: 2 }}></Col>
        </Row>
      </Col>
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
    </Row>
  );
};
