import { Col, Row } from "antd";
export default ({ children, extraStyle }) => {
  return (
    <Row
      style={{
        width: "100%",
        backgroundColor: "#121212",
        marginTop: "25vh",
        paddingBottom: "25vh",
        ...extraStyle,
      }}
    >
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
      <Col xs={{ span: 22 }} lg={{ span: 18 }}>
        {children}
      </Col>
      {/* CONTENT GOES HERE */}
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
    </Row>
  );
};
