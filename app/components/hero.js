import { Button, Col, Row } from "antd";

export default ({ screens, isMobile }) => {
  return (
    <Row
      style={{
        width: "100%",
        padding: 0,
        marginLeft: 0,
        marginRight: 0,
        backgroundImage: "url('https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_2442/v1646866037/hero-img-1_gj4jch.png')",
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        borderBottom: "1px solid rgba(225,225,225,.3)",
      }}
    >
      <Col xs={{ span: 1 }} lg={{ span: 3 }} style={{}}></Col>
      <Col
        xs={{ span: 22 }}
        lg={{ span: 18 }}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {/* CONTENT GOES HERE */}
        <Row
          gutter={[16, 16]}
          style={{
            marginLeft: 0,
            marginRight: 0,
            padding: 0,
            width: "100%",
            minHeight: "calc(100vh - 92px)",
            paddingTop: isMobile ? 80 : 0,
            paddingBottom: isMobile ? 80 : 0,
          }}
          justify="center"
          align="middle"
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 14 }}
            style={{
              // height: "100%",
              display: "flex",
              flexFlow: "column",
              alignItems: "start",
              justifyContent: "center",
              // paddingTop: "10%",
            }}
          >
            <h1 className="hero-title">
              <b>C</b>onvexity
              <br /> Wrapped CBDC{" "}
            </h1>
            <br />
            <br />
            <span className="hero-subtitle">
              Bringing African CBDCs to your favorite
              <br /> blockchain.
            </span>
            <br />
            {/* <br/> */}
            <Button
              type="link"
              size="large"
              style={{
                backgroundColor: "transparent",
                color: "white",
                borderColor: "white",
              }}
              href="https://wrapcbdc-dashboard-staging.herokuapp.com/?view=signup"
            >
              Get CNGN
            </Button> 
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 10 }}
            style={
              {
                // height: "100%",
                // display: "flex",
                // flexFlow: "column",
                // alignItems: "start",
                // justifyContent: "center",
                // paddingTop: "5%",
                // paddingBottom: "5%",
              }
            }
          >
            <img src="https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_1060/v1646866079/hero-token_hri4r4.png" width={"100%"} height={"100%"} />
          </Col>
        </Row>
      </Col>
      <Col xs={{ span: 1 }} lg={{ span: 3 }} style={{}}></Col>
    </Row>
  );
};
