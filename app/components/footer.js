import { Button, Col, Input, Row } from "antd";

export default ({ screens, isMobile }) => {
  return (
    <Row
      style={{
        width: "100%",
        marginTop: 140,
        paddingBottom: 140,
        backgroundImage: "url('https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_1649/v1646896289/world_map_usjcvp.png')",
        backgroundPosition: "center -100%",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
      <Col xs={{ span: 22 }} lg={{ span: 18 }}>
        {/* CONTENT GOES HERE */}
        <Row
          gutter={[8, 32]}
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginBottom: 140,
            padding: 0,
            width: "100%",
          }}
        >
          <Col xs={{ span: 1 }} lg={{ span: 2 }}></Col>
          <Col
            xs={{ span: 22 }}
            lg={{ span: 20 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: "center",
              justifyContent: "start",
            }}
          >
            <h1
              className="footer-title"
              style={{ textAlign: isMobile ? "center" : "left" }}
            >
              WELCOME TO THE NEW WORLD OF POSSIBILITIES ENABLE BY Convexity
              WRAPped cbdc tokens
            </h1>
            <span className="footer-subtitle">
              Get the latest milestones & announcements from Convexity.
            </span>
            <br />
            <br />
            <Input.Group compact style={{ textAlign: "center" }}>
              <Input
                size="large"
                type="email"
                placeholder="Email Address"
                style={{
                  width: isMobile ? "100%" : "calc(100% - 200px)",
                  textAlign: "left",
                }}
              />
              <Button
                type="link"
                size="large"
                style={{
                  backgroundColor: "black",
                  color: "white",
                  border: "1px solid white",
                }}
                block={isMobile}
              >
                Subscribe
              </Button>
            </Input.Group>
          </Col>
          <Col xs={{ span: 1 }} lg={{ span: 2 }}></Col>
        </Row>
        <Row
          gutter={[16, 16]}
          style={{
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: isMobile ? "center" : "start",
              justifyContent: "space-evenly",
              height: "100%",
            }}
          >
            <img src="/logo_edited.png" width={90} height={90} />
            <br />
            <h1
              style={{
                margin: 0,
                color: "white",
                fontFamily: "Poppins-Bold",
              }}
            >
              WRAP CBDC
            </h1>
            <br />
            <div
              style={{
                display: "flex",
                flexFlow: "row",
                alignItems: "center",
                justifyContent: isMobile ? "center" : "start",
                width: "100%",
                paddingRight: isMobile ? 0 : 20,
              }}
            >
              <img
                src="/footer_assets/socials/linkdin.png"
                width={20}
                height={20}
                style={{ marginRight: 15 }}
              />
              <img
                src="/footer_assets/socials/twitter.png"
                width={20}
                height={20}
                style={{ marginRight: 15 }}
              />
              <img
                src="/footer_assets/socials/medium.png"
                width={20}
                height={20}
                style={{ marginRight: 15 }}
              />
              <img
                src="/footer_assets/socials/mail.png"
                width={30}
                height={20}
              />
            </div>
            <br />
            <span
              style={{
                color: "white",
                textAlign: "left",
              }}
            >
              +234-1234-56-7890
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: isMobile ? "center" : "start",
              justifyContent: "space-evenly",
              height: "100%",
              paddingTop: 30,
            }}
          >
            <h3
              style={{
                fontFamily: "Poppins-Bold",
                margin: 0,
                color: "white",
              }}
            >
              Company
            </h3>
            <br />
            <span>
              <a className="footer-link">Whitepaper</a>
            </span>
            <span>
              <a className="footer-link">Privacy Policy</a>
            </span>
            <span>
              <a className="footer-link">Terms & condition</a>
            </span>
            <span>
              <a className="footer-link">Disclaimer</a>
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: isMobile ? "center" : "start",
              justifyContent: "space-evenly",
              height: "100%",
              paddingTop: 30,
            }}
          >
            <h3
              style={{
                fontFamily: "Poppins-Bold",
                margin: 0,
                color: "white",
              }}
            >
              Stablecoin
            </h3>
            <br />
            <span>
              <a className="footer-link" href="/fees">
                Fees
              </a>
            </span>
            <span>
              <a className="footer-link">Deposit</a>
            </span>
            <span>
              <a className="footer-link">Withdrawal & AML policy</a>
            </span>
            <span>
              <a className="footer-link" href="/verification">
                Verification
              </a>
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 6 }}
            style={{
              display: "flex",
              flexFlow: "column",
              alignItems: isMobile ? "center" : "start",
              justifyContent: "space-evenly",
              height: "100%",
              paddingTop: 30,
            }}
          >
            <h3
              style={{
                fontFamily: "Poppins-Bold",
                margin: 0,
                color: "white",
              }}
            >
              Support
            </h3>
            <br />
            <span>
              <a className="footer-link" href="/contact">
                Contact Us
              </a>
            </span>
            <span>
              <a className="footer-link" href="/faq">
                Faq`s
              </a>
            </span>
            <span>
              <a className="footer-link">Annoouncements</a>
            </span>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 24 }}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "100%",
              paddingTop: 30,
            }}
          >
            <span className="footer-link">
              © 2022 wrapcbdc.com. All rights reserved.
            </span>
          </Col>
        </Row>
      </Col>
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
    </Row>
  );
};
