import { CloseOutlined, GlobalOutlined, MenuOutlined } from "@ant-design/icons";
import { Button, Menu } from "antd";
import { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "remix";

// const { useBreakpoint } = Grid;

export default ({ isMobile, screens }) => {
  const navigate = useNavigate();
  const location = useLocation();
  const { pathname } = location;
  // const { screens, isMobile } = useOutletContext();
  const [isMenuVisible, setIsMenuVisible] = useState(false);
  const [menuKey, setMenuKey] = useState(
    pathname.includes("fees")
      ? "fees"
      : pathname.includes("contact")
      ? "contact"
      : pathname === "/"
      ? "home"
      : null
  );

  useEffect(() => {
    setIsMenuVisible(false);
    setMenuKey(
      pathname.includes("fees")
        ? "fees"
        : pathname.includes("contact")
        ? "contact"
        : pathname === "/"
        ? "home"
        : null
    );

    return () => {
      setIsMenuVisible(false);
    };
  }, [pathname, screens]);

  return (
    <>
      <Link to="/">
        <div className="logo" />
      </Link>
      {!isMobile && (
        <div
          style={{
            flex: 3,
            display: "flex",
            alignItems: "center",
            justifyContent: "space-around",
            textAlign: "right",
            // paddingTop: 20,
          }}
        >
          <Menu
            theme="dark"
            mode="horizontal"
            // defaultSelectedKeys={[menuKey]}
            selectedKeys={[menuKey]}
            style={{
              flex: 1,
              marginRight: 20,
              backgroundColor: "transparent",
              float: "right",
            }}
            onClick={({ item, key, keyPath, domEvent }) => {
              console.log(item, key, keyPath, domEvent);
              setMenuKey(key);
              navigate(key !== "home" ? `${key}` : "/");
            }}
          >
            <Menu.Item key="home">Home</Menu.Item>
            <Menu.Item key="fees">Fees</Menu.Item>
            <Menu.Item key="contact">Contact</Menu.Item>
          </Menu>
          <Button
            type="link"
            size="large"
            style={{
              backgroundColor: "transparent",
              color: "white",
              fontFamily: "Poppins-Light",
              float: "right",
              marginRight: 10,
            }}
            href="https://wrapcbdc-dashboard-staging.herokuapp.com/?view=login"
          >
            Sign in
          </Button>
          <Button
            type="link"
            size="large"
            style={{
              backgroundColor: "white",
              color: "black",
              fontFamily: "Poppins-Light",
              float: "right",
            }}
            href="https://wrapcbdc-dashboard-staging.herokuapp.com/?view=signup"
          >
            Get CNGN
          </Button>
          <Button
            type="link"
            size="large"
            style={{
              color: "white",
              fontFamily: "Poppins",
              float: "right",
            }}
            icon={<GlobalOutlined style={{ color: "white" }} />}
          >
            Eng
          </Button>
        </div>
      )}
      {isMobile && !isMenuVisible && (
        <MenuOutlined
          style={{ color: "white", fontSize: 21 }}
          onClick={() => setIsMenuVisible((visible) => !visible)}
        />
      )}
      {isMobile && isMenuVisible && (
        <CloseOutlined
          style={{ color: "white", fontSize: 21 }}
          onClick={() => setIsMenuVisible((visible) => !visible)}
        />
      )}
      {isMobile && isMenuVisible && (
        <div
          style={{
            flex: 3,
            display: "flex",
            flexFlow: "column",
            alignItems: "center",
            justifyContent: "space-around",
            position: "absolute",
            top: 84,
            right: 20,
            left: 20,
            backgroundColor: "#121212",
            zIndex: 100,
            // textAlign: "right",
            // paddingTop: 20,
          }}
        >
          <Menu
            theme="dark"
            mode="vertical"
            // defaultSelectedKeys={[menuKey]}
            selectedKeys={[menuKey]}
            style={{
              flex: 1,
              // marginRight: 20,
              width: "100%",
              backgroundColor: "transparent",
              textAlign: "center",
            }}
            onClick={({ item, key, keyPath, domEvent }) => {
              console.log(item, key, keyPath, domEvent);
              setMenuKey(key);
              navigate(key !== "home" ? `${key}` : "/");
            }}
          >
            <Menu.Item key="home">Home</Menu.Item>
            <Menu.Item key="fees">Fees</Menu.Item>
            <Menu.Item key="contact">Contact</Menu.Item>
          </Menu>
          <Button
            type="link"
            size="large"
            style={{
              backgroundColor: "transparent",
              color: "white",
              fontFamily: "Poppins-Light",
              float: "right",
              marginRight: 10,
            }}
            href="https://wrapcbdc-dashboard-staging.herokuapp.com/?view=login"
            block
          >
            Sign in
          </Button>
          <Button
            type="link"
            size="large"
            style={{
              backgroundColor: "white",
              color: "black",
              fontFamily: "Poppins-Light",
              float: "right",
            }}
            href="https://wrapcbdc-dashboard-staging.herokuapp.com/?view=signup"
            block
          >
            Get CNGN
          </Button>
          <Button
            type="link"
            size="large"
            style={{
              color: "white",
              fontFamily: "Poppins",
              float: "right",
            }}
            icon={<GlobalOutlined style={{ color: "white" }} />}
            block
          >
            Eng
          </Button>
        </div>
      )}
    </>
  );
};
