import { Button, Col, Collapse, Row } from "antd";
const { Panel } = Collapse;

export default () => {
  return (
    <Row style={{ width: "100%", backgroundColor: "#212121" }}>
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
      <Col xs={{ span: 22 }} lg={{ span: 18 }}>
        {/* CONTENT GOES HERE */}
        <Row
          gutter={[16, 16]}
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginTop: 140,
            marginBottom: 140,
            padding: 0,
            width: "100%",
          }}
        >
          <Col
            xs={{ span: 24 }}
            lg={{ span: 10 }}
            style={{
              height: "100%",
              display: "flex",
              flexFlow: "column",
              alignItems: "start",
              justifyContent: "space-evenly",
            }}
          >
            <img src="/sec4_assets/faq.png" width={300} height={220} />
            <br />
            <h1 className="sec4-title">
              You have questions?
              <br />
              We have answers.
            </h1>
            <span className="sec4-subtitle">
            Our FAQs can sort out any enquries or issues that you have got. 
            We have also got a team of experts assigned to handle your issues promptly.
            </span>
            <br />
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Button
                type="link"
                size="large"
                style={{ backgroundColor: "white", color: "black" }}
                href="/faq"
              >
                READ MORE FAQ
              </Button>
            </div>
          </Col>
          <Col
            xs={{ span: 24 }}
            lg={{ span: 14 }}
            style={{
              height: "100%",
              display: "flex",
              flexFlow: "column",
              alignItems: "end",
              justifyContent: "center",
              paddingTop: "10%",
            }}
          >
            <Collapse
              defaultActiveKey={["1"]}
              onChange={() => {}}
              expandIconPosition={"right"}
              bordered={false}
              className="sec4-collapse"
            >
              <Panel
                header="What is Convexity token?"
                key="1"
                className="sec4-panel"
              >
                <div></div>
              </Panel>
              <Panel header="How does it work?" key="2">
                <div></div>
              </Panel>
              <Panel header="Who can use it?" key="3">
                <div></div>
              </Panel>
              <Panel
                header="How does Convexity wrap token protect me from crypto volatility?"
                key="4"
              >
                <div></div>
              </Panel>
              <Panel
                header="How do i know my  Convexity wrap token are secured?"
                key="5"
              >
                <div></div>
              </Panel>
              <Panel header="Is Convexity wrap token transparent?" key="6">
                <div></div>
              </Panel>
              {/* <Panel header="Is Convexity wrap token transparent?" key="3">
                <div></div>
              </Panel>
              <Panel header="Is Convexity wrap token transparent?" key="3">
                <div></div>
              </Panel> */}
            </Collapse>
          </Col>
        </Row>
      </Col>
      <Col xs={{ span: 1 }} lg={{ span: 3 }}></Col>
    </Row>
  );
};
