import { BackTop, Grid, Layout } from "antd";
import antdStyle from "antd/dist/antd.min.css";
import { useEffect, useState } from "react";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration, useLocation
} from "remix";
import Footer from "~/components/footer";
import Navigation from "~/components/navigation";
import globalStyles from "~/styles/global.css";

const { Content, Header } = Layout;
const { useBreakpoint } = Grid;

export function meta() {
  return { title: "Wrapped CBDC - Landing page" };
}

export const links = () => {
  return [{ rel: "stylesheet", href: antdStyle }, { rel: "stylesheet", href: globalStyles }];
};

export default function App() {
  const screens = useBreakpoint();
  const location = useLocation();
  const { pathname } = location;
  console.log(pathname, "Pathname");

  const [isMobile, setIsMobile] = useState(
    !screens.xxl && !screens.xl && !screens.lg
  );

  useEffect(() => {
    setIsMobile(!screens.xxl && !screens.xl && !screens.lg);
    console.log(isMobile, "Mobile View Screen");
    return () => {};
  }, [screens]);

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="icon" type="image/png" href="/logo_edited.png"/>
        <Meta />
        <Links />
      </head>
      <body>
        <Layout style={{minHeight: "100vh", backgroundColor: "#121212"}}>
          <Header style={{height: "auto", display: "flex", alignItems: "center", justifyContent: "space-between", background: "transparent", backgroundImage: pathname === '/'? "url('https://res.cloudinary.com/convexity/image/upload/c_scale,f_auto,q_auto,w_2442/v1646866037/hero-img-1_gj4jch.png')" : undefined,
            backgroundPosition: "top center",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            position: 'relative'
          }}>
            <Navigation isMobile={isMobile} screens={screens} />
          </Header>
          <Content>
            {/* <Row style={{width: "100%"}}>
              <Col xs={{span: 1}} lg={{span: 3}}></Col>
              <Col xs={{span: 22}} lg={{span: 18}}> */}
                <Outlet context={{screens, isMobile}} />
              {/* </Col>
              <Col xs={{span: 1}} lg={{span: 3}}></Col>
            </Row> */}
          </Content>
          <Footer isMobile={isMobile} screens={screens} />
          <BackTop />
        </Layout>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
